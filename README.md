Originally from https://github.com/bemasher/rtl-sdr

# rtl-sdr

A Docker container for building and executing rtl-sdr tools: https://osmocom.org/projects/rtl-sdr/wiki/Rtl-sdr

# Usage

Obtain the image from the docker hub:

```update with gitlab pull link```

Run an rtl-sdr tool from the container:

```docker run -d -p 1234:1234 --privileged -v /dev/bus/usb:/dev/bus/usb --name rtl_tcp bertabus/rtl-sdr rtl_tcp -a 0.0.0.0```
